package com.dev.fi.footballschedule.di

import com.dev.fi.footballschedule.data.rest.Repository
import com.dev.fi.footballschedule.data.rest.RepositoryImp
import com.dev.fi.footballschedule.ui.detailMatch.DetailMatchActivityModelView
import com.dev.fi.footballschedule.ui.listLeague.ListLeagueActivityModelView
import com.dev.fi.footballschedule.ui.listMatch.MatchFragmentModelView
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

val footballscheduleViewModelModule = module {
    // TODO define view model to inject
    viewModel { ListLeagueActivityModelView(get()) }
    viewModel { MatchFragmentModelView(get()) }
    viewModel { DetailMatchActivityModelView(get()) }
}


val dataModule = module(createOnStart = true) {
    single<Repository> { RepositoryImp(get()) }
}


val footballschedule = listOf(footballscheduleViewModelModule, dataModule, networkModule)