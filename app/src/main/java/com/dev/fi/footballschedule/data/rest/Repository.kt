package com.dev.fi.footballschedule.data.rest

import com.dev.fi.footballschedule.data.model.AllLeague
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.data.model.TeamDetail
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.HeaderMap

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

interface Repository {
    fun getAllLeague(): Flowable<Response<AllLeague.Leagues>>
    fun getPastLeague(@HeaderMap headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>>
    fun getNextLeague(@HeaderMap headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>>
    fun getTeamDetail(@HeaderMap headers: LinkedHashMap<String, String>): Flowable<Response<TeamDetail.Teams>>
}

class RepositoryImp(private val api: Api) : Repository {
    override fun getAllLeague(): Flowable<Response<AllLeague.Leagues>> = api.allLeagues()
    override fun getPastLeague(headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>> = api.eventsPastLeague(headers)
    override fun getNextLeague(headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>> = api.eventsNextLeague(headers)
    override fun getTeamDetail(headers: LinkedHashMap<String, String>): Flowable<Response<TeamDetail.Teams>> = api.teamDetail(headers)
}