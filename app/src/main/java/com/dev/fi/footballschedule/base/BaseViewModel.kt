package com.dev.fi.footballschedule.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

abstract class BaseViewModel : ViewModel() {
    private val composite = CompositeDisposable()

    fun composite(job: () -> Disposable) {
        composite.add(job())
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        composite.clear()
    }
}