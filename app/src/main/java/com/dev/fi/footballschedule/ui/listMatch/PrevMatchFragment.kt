package com.dev.fi.footballschedule.ui.listMatch

import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.base.BaseFragment
import com.dev.fi.footballschedule.databinding.FragmentMatchBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class PrevMatchFragment : BaseFragment<FragmentMatchBinding>() {
    private val viewModel by viewModel<MatchFragmentModelView>()

    override fun getLayoutResource(): Int = R.layout.fragment_match

    override fun mainCode() {
        dataBinding.setLifecycleOwner(this)

        dataBinding.viewModel = viewModel
        val id = activity.intent.extras.get("id") as String

        viewModel.getListMatch(activity.applicationContext, id, "prev")

        dataBinding.rvMatch.layoutManager = LinearLayoutManager(activity)
        dataBinding.rvMatch.setHasFixedSize(true)
    }

}