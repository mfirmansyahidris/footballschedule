package com.dev.fi.footballschedule.base

import android.app.Application
import com.dev.fi.footballschedule.di.footballschedule
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import org.koin.android.ext.android.startKoin

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)
                .methodCount(2)
                .methodOffset(5)
                .tag("FootballScheduleLog")
                .build()
        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))

        startKoin(this, footballschedule)
    }
}
