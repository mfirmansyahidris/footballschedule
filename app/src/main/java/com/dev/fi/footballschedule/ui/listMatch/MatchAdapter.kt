package com.dev.fi.footballschedule.ui.listMatch

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.databinding.ItemMatchBinding
import com.dev.fi.footballschedule.ui.detailMatch.DetailMatchActivity
import com.orhanobut.logger.Logger

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class MatchAdapter: RecyclerView.Adapter<MatchAdapter.ViewHolder>(){
    private var items: List<MatchEvent.Event>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            = ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_match, parent, false))

    override fun getItemCount(): Int = items?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items!![position])

    fun updateList(itemList: List<MatchEvent.Event>?) {
        items = itemList ?: throw NullPointerException()
        Logger.d("data insert")
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemMatchBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: MatchEvent.Event) = with(itemView){
            binding.dataMatchs = data
            setOnClickListener {
                val intentDetail = Intent(itemView.context, DetailMatchActivity::class.java)
                intentDetail.putExtra("event", data)
                context.startActivity(intentDetail)
            }
        }
    }
}