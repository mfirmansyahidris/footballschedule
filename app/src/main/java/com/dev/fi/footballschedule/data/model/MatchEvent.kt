package com.dev.fi.footballschedule.data.model

import android.os.Parcel
import android.os.Parcelable

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class MatchEvent {
    data class Events(var events: List<Event>?)
    data class Event(
            var idEvent: String? = "",
            var idSoccerXML: String? = "",
            var strEvent: String? = "",
            var strFilename: String? = "",
            var strSport: String? = "",
            var idLeague: String? = "",
            var strLeague: String? = "",
            var strSeason: String? = "",
            var strDescriptionEN: String? = "",
            var strHomeTeam: String? = "",
            var strAwayTeam: String? = "",
            var intHomeScore: String? = "",
            var intRound: String? = "",
            var intAwayScore: String? = "",
            var intSpectators: String? = "",
            var strHomeGoalDetails: String? = "",
            var strHomeRedCards: String? = "",
            var strHomeYellowCards: String? = "",
            var strHomeLineupGoalkeeper: String? = "",
            var strHomeLineupDefense: String? = "",
            var strHomeLineupMidfield: String? = "",
            var strHomeLineupForward: String? = "",
            var strHomeLineupSubstitutes: String? = "",
            var strHomeFormation: String? = "",
            var strAwayRedCards: String? = "",
            var strAwayYellowCards: String? = "",
            var strAwayGoalDetails: String? = "",
            var strAwayLineupGoalkeeper: String? = "",
            var strAwayLineupDefense: String? = "",
            var strAwayLineupMidfield: String? = "",
            var strAwayLineupForward: String? = "",
            var strAwayLineupSubstitutes: String? = "",
            var strAwayFormation: String? = "",
            var intHomeShots: String? = "",
            var intAwayShots: String? = "",
            var dateEvent: String? = "",
            var strDate: String? = "",
            var strTime: String? = "",
            var strTVStation: String? = "",
            var idHomeTeam: String? = "",
            var idAwayTeam: String? = "",
            var strResult: String? = "",
            var strCircuit: String? = "",
            var strCountry: String? = "",
            var strCity: String? = "",
            var strPoster: String? = "",
            var strFanart: String? = "",
            var strThumb: String? = "",
            var strBanner: String? = "",
            var strMap: String? = "",
            var strLocked: String? = ""
    ) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString()) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(idEvent)
            parcel.writeString(idSoccerXML)
            parcel.writeString(strEvent)
            parcel.writeString(strFilename)
            parcel.writeString(strSport)
            parcel.writeString(idLeague)
            parcel.writeString(strLeague)
            parcel.writeString(strSeason)
            parcel.writeString(strDescriptionEN)
            parcel.writeString(strHomeTeam)
            parcel.writeString(strAwayTeam)
            parcel.writeString(intHomeScore)
            parcel.writeString(intRound)
            parcel.writeString(intAwayScore)
            parcel.writeString(intSpectators)
            parcel.writeString(strHomeGoalDetails)
            parcel.writeString(strHomeRedCards)
            parcel.writeString(strHomeYellowCards)
            parcel.writeString(strHomeLineupGoalkeeper)
            parcel.writeString(strHomeLineupDefense)
            parcel.writeString(strHomeLineupMidfield)
            parcel.writeString(strHomeLineupForward)
            parcel.writeString(strHomeLineupSubstitutes)
            parcel.writeString(strHomeFormation)
            parcel.writeString(strAwayRedCards)
            parcel.writeString(strAwayYellowCards)
            parcel.writeString(strAwayGoalDetails)
            parcel.writeString(strAwayLineupGoalkeeper)
            parcel.writeString(strAwayLineupDefense)
            parcel.writeString(strAwayLineupMidfield)
            parcel.writeString(strAwayLineupForward)
            parcel.writeString(strAwayLineupSubstitutes)
            parcel.writeString(strAwayFormation)
            parcel.writeString(intHomeShots)
            parcel.writeString(intAwayShots)
            parcel.writeString(dateEvent)
            parcel.writeString(strDate)
            parcel.writeString(strTime)
            parcel.writeString(strTVStation)
            parcel.writeString(idHomeTeam)
            parcel.writeString(idAwayTeam)
            parcel.writeString(strResult)
            parcel.writeString(strCircuit)
            parcel.writeString(strCountry)
            parcel.writeString(strCity)
            parcel.writeString(strPoster)
            parcel.writeString(strFanart)
            parcel.writeString(strThumb)
            parcel.writeString(strBanner)
            parcel.writeString(strMap)
            parcel.writeString(strLocked)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Event> {
            override fun createFromParcel(parcel: Parcel): Event {
                return Event(parcel)
            }

            override fun newArray(size: Int): Array<Event?> {
                return arrayOfNulls(size)
            }
        }
    }
}