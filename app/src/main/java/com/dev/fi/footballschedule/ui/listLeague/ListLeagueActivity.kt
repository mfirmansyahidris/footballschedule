package com.dev.fi.footballschedule.ui.listLeague

import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.base.BaseActivity
import com.dev.fi.footballschedule.databinding.ActivityListLeagueBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListLeagueActivity : BaseActivity<ActivityListLeagueBinding>() {
    private val viewModel by viewModel<ListLeagueActivityModelView>()

    override fun getLayoutResource(): Int = R.layout.activity_list_league

    override fun getToolbarResource(): Int = 0

    override fun getToolbarTitle(): String = ""

    override fun mainCode() {
        dataBinding.setLifecycleOwner(this)
        toolbar = findViewById(R.id.main_toolbar)
        toolbar?.setTitle(getString(R.string.title_listLeagues))

        dataBinding.viewModel = viewModel
        viewModel.getListLeague(applicationContext)

        dataBinding.rvLeague.layoutManager = LinearLayoutManager(this)
        dataBinding.rvLeague.setHasFixedSize(true)

    }


}
