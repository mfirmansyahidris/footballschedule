package com.dev.fi.footballschedule.ui.listLeague

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.dev.fi.footballschedule.base.BaseViewModel
import com.dev.fi.footballschedule.data.rest.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class ListLeagueActivityModelView(private val repository: Repository) : BaseViewModel() {
    val adapter: LeagueAdapter = LeagueAdapter()
    var visibilityProgressBar = MutableLiveData<Boolean>()

    fun getListLeague(context: Context) {
        composite {
            repository.getAllLeague()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { visibilityProgressBar.value = true }
                    .doOnComplete { visibilityProgressBar.value = false }
                    .subscribe({
                        if (it.code() == 200) {
                            adapter.updateList(it.body()?.leagues)
                        }
                    }, {
                        Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}